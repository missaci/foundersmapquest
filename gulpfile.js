/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';
const gulp          = require("gulp");
const sourcemaps    = require("gulp-sourcemaps");
const babel         = require("gulp-babel");
const concat        = require("gulp-concat");
const uglify        = require("gulp-uglify");
const stylish       = require('jshint-stylish');
const jshint        = require('gulp-jshint');
const gulpSequence  = require( 'gulp-sequence' );
const combiner      = require('stream-combiner2');
const minifyHtml    = require('gulp-minify-html');
const server        = require('gulp-webserver');
const watch         = require('gulp-watch');
const webpack       = require('webpack-stream');
const path          = require('path');

gulp.task( "default", gulpSequence( "lint", [ "build", "moveVendors" ] ) );

gulp.task("build", function () {
    return gulp.src("src/scripts/**/*.js")
        //.pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(webpack({module:{loaders: [{loader: "babel-loader"}]}}))
        //.pipe(sourcemaps.write("."))
        .pipe(concat("main.js"))
        //.pipe(uglify())
        .pipe(gulp.dest("www/scripts"));
});

gulp.task("lint", function () {
    return gulp.src("src/scripts/**/*.js")
        .pipe(jshint())
        .pipe(jshint.reporter(stylish));
});

gulp.task("moveVendors", function() {
    let combined = combiner.obj([
        gulp.src("src/bower_components/**/*"),
        gulp.dest("www/bower_components/")
    ]);
    combined.on('error', console.error.bind(console));
    return combined;
});

gulp.task('buildMarkup', function() {
    var combined = combiner.obj([
        gulp.src('src/**/*.html'),
        minifyHtml({
            empty: true,
            conditionals: true,
            quotes: true,
            comments: true
        }),
        gulp.dest('www/' )
    ]);
    combined.on('error', console.error.bind(console));
    return combined;
} );

gulp.task('server', function() {
    var serverOptions = {
        livereload: true,
        directoryListing: false,
        open: false,
        host: 'localhost',
        port: 5000,
        https: false
    };
    gulp.src( 'www' )
        .pipe( server( serverOptions ) );
} );

gulp.task('watch', [ 'server' ], function() {
    gulp.watch( 'src/**/*.js', [ 'build' ] );
    gulp.watch( 'src/**/*.html', [ 'buildMarkup' ] );
} );