/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';

var mock = {
    "data": [{
        "Id": "                                    1",
        "Company Name": "Google",
        "Founder": "Larry Page & Sergey Brin",
        "City": "Mountain View",
        "Country": "USA",
        "Postal Code": "CA 94043",
        "Street": "1600 Amphitheatre Pkwy",
        "Photo": "http://interviewsummary.com/wp-content/uploads/2013/07/larry-page-and-sergey-brin-of-google-620x400.jpg",
        "Home Page": "http://google.com",
        "Garage Latitude": "37.457674",
        "Garage Longitude": "-122.163452"
    }, {
        "Id": "                                    2",
        "Company Name": "Apple",
        "Founder": "Steve Jobs & Steve Wozniak",
        "City": "Cupertino",
        "Country": "USA",
        "Postal Code": "CA 95014",
        "Street": "1 Infinite Loop",
        "Photo": "http://i.dailymail.co.uk/i/pix/2013/02/08/article-2275512-172E13BB000005DC-732_634x505.jpg",
        "Home Page": "http://apple.com",
        "Garage Latitude": "37.3403188",
        "Garage Longitude": "-122.0581469"
    }, {
        "Id": "                                    3",
        "Company Name": "Microsoft",
        "Founder": "Bill Gates",
        "City": "Redmond",
        "Country": "USA",
        "Postal Code": "WA 98052-7329",
        "Street": "One Microsoft Way",
        "Photo": "http://postdefiance.com/wp-content/uploads/2013/02/bill-gates-microsoft-young.jpg",
        "Home Page": "http://microsoft.com",
        "Garage Latitude": "37.472189",
        "Garage Longitude": "-122.190191"
    }],
    "columns": [{
        "title": "Id",
        "data": "Id",
        "sTitle": "Id",
        "mData": "Id"
    }, {
        "title": "Company Name",
        "data": "Company Name",
        "sTitle": "Company Name",
        "mData": "Company Name"
    }, {
        "title": "Founder",
        "data": "Founder",
        "sTitle": "Founder",
        "mData": "Founder"
    }, {
        "title": "City",
        "data": "City",
        "sTitle": "City",
        "mData": "City"
    }, {
        "title": "Country",
        "data": "Country",
        "sTitle": "Country",
        "mData": "Country"
    }, {
        "title": "Postal Code",
        "data": "Postal Code",
        "sTitle": "Postal Code",
        "mData": "Postal Code"
    }, {
        "title": "Street",
        "data": "Street",
        "sTitle": "Street",
        "mData": "Street"
    }, {
        "title": "Photo",
        "data": "Photo",
        "sTitle": "Photo",
        "mData": "Photo"
    }, {
        "title": "Home Page",
        "data": "Home Page",
        "sTitle": "Home Page",
        "mData": "Home Page"
    }, {
        "title": "Garage Latitude",
        "data": "Garage Latitude",
        "sTitle": "Garage Latitude",
        "mData": "Garage Latitude"
    }, {
        "title": "Garage Longitude",
        "data": "Garage Longitude",
        "sTitle": "Garage Longitude",
        "mData": "Garage Longitude"
    }],
    "aaData": [{
        "Id": "                                    1",
        "Company Name": "Google",
        "Founder": "Larry Page & Sergey Brin",
        "City": "Mountain View",
        "Country": "USA",
        "Postal Code": "CA 94043",
        "Street": "1600 Amphitheatre Pkwy",
        "Photo": "http://interviewsummary.com/wp-content/uploads/2013/07/larry-page-and-sergey-brin-of-google-620x400.jpg",
        "Home Page": "http://google.com",
        "Garage Latitude": "37.457674",
        "Garage Longitude": "-122.163452"
    }, {
        "Id": "                                    2",
        "Company Name": "Apple",
        "Founder": "Steve Jobs & Steve Wozniak",
        "City": "Cupertino",
        "Country": "USA",
        "Postal Code": "CA 95014",
        "Street": "1 Infinite Loop",
        "Photo": "http://i.dailymail.co.uk/i/pix/2013/02/08/article-2275512-172E13BB000005DC-732_634x505.jpg",
        "Home Page": "http://apple.com",
        "Garage Latitude": "37.3403188",
        "Garage Longitude": "-122.0581469"
    }, {
        "Id": "                                    3",
        "Company Name": "Microsoft",
        "Founder": "Bill Gates",
        "City": "Redmond",
        "Country": "USA",
        "Postal Code": "WA 98052-7329",
        "Street": "One Microsoft Way",
        "Photo": "http://postdefiance.com/wp-content/uploads/2013/02/bill-gates-microsoft-young.jpg",
        "Home Page": "http://microsoft.com",
        "Garage Latitude": "37.472189",
        "Garage Longitude": "-122.190191"
    }],
    "aoColumns": [{
        "title": "Id",
        "data": "Id",
        "sTitle": "Id",
        "mData": "Id"
    }, {
        "title": "Company Name",
        "data": "Company Name",
        "sTitle": "Company Name",
        "mData": "Company Name"
    }, {
        "title": "Founder",
        "data": "Founder",
        "sTitle": "Founder",
        "mData": "Founder"
    }, {
        "title": "City",
        "data": "City",
        "sTitle": "City",
        "mData": "City"
    }, {
        "title": "Country",
        "data": "Country",
        "sTitle": "Country",
        "mData": "Country"
    }, {
        "title": "Postal Code",
        "data": "Postal Code",
        "sTitle": "Postal Code",
        "mData": "Postal Code"
    }, {
        "title": "Street",
        "data": "Street",
        "sTitle": "Street",
        "mData": "Street"
    }, {
        "title": "Photo",
        "data": "Photo",
        "sTitle": "Photo",
        "mData": "Photo"
    }, {
        "title": "Home Page",
        "data": "Home Page",
        "sTitle": "Home Page",
        "mData": "Home Page"
    }, {
        "title": "Garage Latitude",
        "data": "Garage Latitude",
        "sTitle": "Garage Latitude",
        "mData": "Garage Latitude"
    }, {
        "title": "Garage Longitude",
        "data": "Garage Longitude",
        "sTitle": "Garage Longitude",
        "mData": "Garage Longitude"
    }]
};


const GMap = require('../domain/map');
const config = require('../infrastructure/config');
const request = require('../infrastructure/request');
const CustomPromise = require('../infrastructure/customPromise');
const GMapsURL = "https://maps.googleapis.com/maps/api/js?key=" + config.get('GMapApiKey');

function MapController() {
    var self = this;
    let promise = new CustomPromise();
    let controls = {
        load: () => promise,
        refreshMarkers: refreshMarkers,
        goTo: goTo,
        map: {}
    };

    function getCurrentPosition() {
        let promise = new CustomPromise();
        navigator.geolocation.getCurrentPosition( geoPosition => promise.resolve(geoPosition.coords) );
        return promise;
    }

    function loadGoogleMapsScript() {
        return request.getScript(GMapsURL);
    }

    function addMarker(data) {
        data.position = (data["Garage Latitude"], data["Garage Longitude"]);
        return self.map.addMarker(data);
    }

    function removeMarker() {
        self.map.removeMarker(data);

    }

    function goTo(marker) {
        self.map.map.panTo(marker.position);
    }

    function refreshMarkers(data) {
        data.data.forEach( (obj, idx) => {
            const marker = addMarker(obj);
            if (idx + 1 === data.data.length) {
                goTo(marker);
            }
        } );
    }

    loadGoogleMapsScript()
        .then( getCurrentPosition )
        .then( coords => {
            self.map = new GMap( config.get('mapElement'), google, {coords: coords} );
            controls.map = self.map.map;
            promise.resolve(self);
        } )
        .catch( (e) => {
            console.log('Error on loading Google Maps service.', e);
            promise.reject( e => console.log(e) );
        } );

    return controls;
}

module.exports = MapController;