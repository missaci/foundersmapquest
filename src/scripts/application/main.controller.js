/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';

const MapController = require('./map.controller');
const uiComponents = require('../infrastructure/uiComponents');
const CustomPromise = require('../infrastructure/customPromise');

function loadDependencies() {
    var promise = new CustomPromise();
    uiComponents.preLoad()
        .then( () => promise.resolve() )
        .catch( e => promise.reject(e) );

    return promise;
}

function startApplication() {

    loadDependencies()
        .then( () => {
            const mapController = new MapController();

            uiComponents.loadSpinner.show();
            mapController.load()
                .then( mapController => mapController.map.on( 'ready', () => uiComponents.loadSpinner.hide() ) )
                .catch( e => console.log(e) );
            uiComponents.csvWizard
                .show()
                .on('applyMarkers', c => mapController.refreshMarkers(c) );

        } )
        .catch( e => {
            uiComponents.loadSpinner.hide();
            console.log(e);
        } );
}

module.exports = {
    startApplication: startApplication
};