'use strict';
const CustomPromise = require('../infrastructure/customPromise');
const request = require('../infrastructure/request');
const config = require('./config');
const $ = global.$;

function template() {

    let templateCache = {};

    function get(template) {
        let promise = new CustomPromise();

        if ( !template ) {
            promise.reject('Tryied to load a template without specifying which one.');
            return promise;
        }

        if( templateCache[template] ) {
            promise.resolve(templateCache[template]);
            return promise;
        }

        request.get('scripts/views/' + template + '.template.html')
            .then( data => {
                try {
                    templateCache[template] = $.parseHTML(data);
                    promise.resolve(templateCache[template]);
                } catch (e) {
                    promise.reject(e);
                }
            })
            .catch( e => promise.reject(e) );
        return promise;
    }

    function preLoad(templates) {
        var promise = new CustomPromise();
        switch (typeof templates) {
            case 'undefined' :
                templates = config.get('templates');
                break;
            case 'string':
                templates = [templates];
                break;
            case 'object':
                templates = typeof templates.push === 'function' ? templates : [];
                break;
            default:
                templates = [];
        }

        templates.forEach( (template, $idx) => {
            get(template)
                .then( () => {
                        if ( templates.length === $idx +1 ) promise.resolve();
                    } )
                .catch( e => console.log(e) );
        } );

        return promise;
    }

    return {
        get: get,
        preLoad: preLoad
    };
}

module.exports = template();
