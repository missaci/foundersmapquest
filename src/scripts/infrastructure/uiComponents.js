/**
 * Created by Philipe on 15/03/2016.
 */
'use strict';

const template = require('../infrastructure/templateHandler');
const CustomPromise = require('../infrastructure/customPromise');
const config = require('../infrastructure/config');
const $ = global.$;

//fix to make bootstrap modal work properly with multiple modals
$.fn.modal.Constructor.prototype.hideModal = function () {
    var self = this;
    this.$element.hide();
    this.backdrop(function () {
        if ( !$('.modal-backdrop').length ) self.$body.removeClass('modal-open');
        self.resetAdjustments();
        self.resetScrollbar();
        self.$element.trigger('hidden.bs.modal');
    });
};

function preLoadComponents() {
    var promise = new CustomPromise();
    template.preLoad()
        .then( () => promise.resolve() )
        .catch( () => promise.reject() );
    return promise;
}

function loadSpinner() {
    let promise = new CustomPromise();
    var spinner = {
        tpl: {},
        show : () => promise.then(()=>$(spinner.tpl).modal('show')),
        hide : () => $(spinner.tpl).modal('hide')
    };

    template.get('loadSpinner')
        .then( tpl => {
            spinner.tpl = $(tpl);
            $('body').append(spinner.tpl);
            promise.resolve(spinner);
        } )
        .catch( e => {
            console.log(e);
            promise.reject(e);
        } );

    return spinner;
}

function csvWizard() {
    let events = {};
    var wizard = {
        csvContentElement: '',
        show: () => { $(wizard.tpl).modal('show'); return wizard; },
        hide: () => $(wizard.tpl).modal('hide'),
        on: on,
        off: off,
        tpl: {},
        val: {
            set: newValue => $('#csvContent').val(newValue),
            get: () => $('#csvContent').val()
        }
    };

    function dispatch(event, data) {
        if ( !event || !events[event] ) return;
        events[event].forEach( callback => { if( typeof callback === 'function' ) callback(data); });
    }

    function on(event, callback) {
        if ( !events[event] ) events[event] = [];
        events[event].push(callback);
    }

    function off(event) {
        if ( events[event] ) delete events[event];
    }

    function applyBinds() {
        const csvContentElement = $('#csvContent');

        $('.nav-tabs li a').on('shown.bs.tab', function(evt){
            const actionButton = $(wizard.tpl).find('.action');
            switch (evt.target.hash) {
                case '#load' :
                    actionButton.off('click').on('click', function(){
                        $('.nav-tabs li a:last').tab('show');
                    } ).text('Load Data!');
                    break;
                case '#manage':
                    const data = datatable(wizard.val.get(), $('[name=separator]:checked').val());
                    actionButton.off('click').on('click', function(){
                        data.title = data["Company Name"];
                        dispatch('applyMarkers', data);
                        wizard.hide();
                    } ).text('Apply Markers!');
                    break;
                default:
                    break;
            }
        });

        csvContentElement.on('change', evt => {
            wizard.val.set(evt.target.value);
            $(wizard.tpl).find('.action').attr('disabled', !evt.target.value);
        } );


        $(wizard.tpl).find('input[type=file]').on('change', event => {
            $(wizard.tpl).find('.form-group.file-wrapper').removeClass('has-error').find('.help-block').text('');
            $('#uploadCsvFileFake').val(event.currentTarget.files[0].name);
            try {
                uploadFile(event.currentTarget.files[0]);
            } catch(e) {
                $(wizard.tpl)
                    .find('.form-group.file-wrapper')
                    .addClass('has-error')
                    .find('.help-block')
                    .text(e);
            }
        } );
    }

    function progressBar() {
        const barWrapper = wizard.tpl.find('.progress');
        const bar = wizard.tpl.find('.progress-bar');

        function update(percentage) {
            percentage += '%';
            bar.css({width: percentage});
            bar.text(percentage);
        }

        return {
            show: () => barWrapper.fadeIn(),
            hide: () => barWrapper.fadeOut(),
            update: update
        };
    }

    function labelHandler() {
        const label = $('#uploadCsvFileLabel');
        let prevLabel = label.text();
        var abortBtn = document.createElement('a');
        abortBtn.textContent = '(abort)';

        return {
            rollback: () => label.text(prevLabel),
            set: (fileName, cb) => {
                abortBtn.onclick = () => cb;
                label.html('Uploading file: ' + fileName + ' - ').append(abortBtn);
            }
        };
    }

    function uploadFile(file) {
        if ( file.size > config.get('csvMaxSize') ) {
            throw Error('File exceeds the max size of ' + config.get('csvMaxSize'));
        } else if ( !file.name.match(/^.*\.((c|t)sv|txt)$/i) ) {
            throw Error('File must be in .csv, .tsv or .txt');
        }

        var reader = new FileReader();
        let progress = progressBar();
        let label = labelHandler();

        label.set(file.name, reader.abort);

        reader.readAsText(file);
        reader.onloadstart = () => progress.show();
        reader.onloadend = () => {
            label.rollback();
            progress.hide();
        };
        reader.onprogress = prog => {
            if (prog.lengthComputable) progress.update(Math.round((prog.loaded * 100) / prog.total));
        };
        reader.onload = evt => wizard.val.set(evt.target.result);
    }

    template.get('wizard')
        .then( tpl => {
            $('body').append(wizard.tpl = $(tpl));
            applyBinds();
            $('.nav-tabs li a:first').tab('show');
        } )
        .catch( e => {
            console.log(e);
        } );

    return wizard;
}

function datatable(csv, separator) {
    let promise = new CustomPromise();
    if ( !csv ) {
        promise.reject('Problem on parsing csv');
        return promise;
    }
    var data = csvParser(csv, separator);
    var tableElement = $('#markersTable');
    const options = {
        data: data.data,
        columns: data.cols
    };

    const footer = tableElement.find('tfoot tr');
    options.columns.forEach( column => footer.append( '<th class=""><input type="text" class="form-control" placeholder="Search ' + column.title + '" /></th>' ) );

    //atualizar a tabela aqui
    var table;
    if ( $.fn.dataTable.isDataTable(tableElement) ) {
        $('#markersTable').DataTable().destroy();
        $('#markersTable thead, #markersTable tbody, #markersTable tfoot tr th').remove();
    }

    table = $('#markersTable').DataTable( options );

    if ( table.columns ) {
        table.columns().every( function () {
            var self = this;
            $( 'input', this.footer() ).on( 'keyup change', function () {
                if ( self.search() !== this.value ) {
                    self.search( this.value ).draw();
                }
            } );
        } );
    }

    return options;
}

function csvParser(data, separator) {

    function markerClassFactory(attributes) {
        return class Marker {
            constructor (data) {
                attributes.forEach(attr => this[attr] = data[attr]);
            }
        };
    }
        var lines = data.split('\n');
        var cols = lines.shift().split(separator).map(attr => attr.trim());
        lines = lines.map(entry => entry.split(separator));
        var objs = [];
        lines.forEach( line => {
            var obj = {};
            cols.forEach((attr, idx) => {
                obj[attr.trim()] = line[idx];
            });
            objs.push(obj);
        });

        var Marker = markerClassFactory(cols);
        var markers = objs.map(obj => new Marker(obj));
        cols = cols.map( col => { return {title: col, data: col}; } );

    return {
        data: markers,
        cols: cols
    };
}

module.exports = {
    preLoad: preLoadComponents,
    loadSpinner: loadSpinner(),
    csvWizard: csvWizard(),
    dataTable: datatable
};