'use strict';

function CustomPromise() {
    var reject, resolve,
        promise = new Promise((y, n) => {
            reject = d => n(d);
            resolve = d => y(d);
        });
    promise.reject = reject;
    promise.resolve = resolve;

    return promise;
}

module.exports = CustomPromise;
