/**
 * Created by Philipe on 15/03/2016.
 */
'use strict';

var config = new Map();
config.set('mapId', 'map');
config.set('GMapApiKey', 'AIzaSyBpE0cAP--pypP4zdUNbJ2Np29zuly9NuQ');
config.set('mapElement', '');
config.set('templates', ['loadSpinner', 'wizard', 'datatable']);
config.set('csvMaxSize', 2 * 1024 * 1024);

module.exports = config;