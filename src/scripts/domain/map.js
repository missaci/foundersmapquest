/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';

const Marker = require('./markers/marker');

class GMap {
    constructor(el, google, options) {
        let events = {};
        let gMapOptions = {
            center: {lat: options.coords.latitude, lng: options.coords.longitude},
            zoom: 12
        };

        function dispatch(event) {
            if ( !event || !events[event] ) return;
            events[event].forEach( callback => { if( typeof callback === 'function' ) callback.call(this); });
        }

        this.events = events;
        this.map = new google.maps.Map(el, gMapOptions);
        this.markers = [];
        this.google = google;

        google.maps.event.addListener( this.map, 'tilesloaded', () => dispatch('ready') );

        return this;
    }

    on(event, callback) {
        if ( !this.events[event] ) this.events[event] = [];
        this.events[event].push(callback);
    }

    off(event) {
        if ( this.events[event] ) delete this.events[event];
    }

    addMarker(data) {
        data.map = this.map;
        data.title = data["Company Name"];
        data.position = {lat: parseFloat(data["Garage Latitude"]), lng: parseFloat(data["Garage Longitude"])};
        const marker = new Marker(data, this.google);
        this.markers.push(marker);

        return marker;
    }

    removeMarker(data) {

    }
}

module.exports = GMap;