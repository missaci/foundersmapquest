/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';

const MarkerOptions = require('./marker.options');

class Marker {
    constructor(options, google) {

        if (options.position) {
            options.position = new google.maps.LatLng(options.position);
        }

        let events = {};
        let markerOptions = new MarkerOptions(options);

        function dispatch(event) {
            if ( !event ) return;
            events[event].forEach( callback => { if( typeof callback === 'function' ) callback.call(this); });
        }

        return new google.maps.Marker(markerOptions);
    }

    on(event, callback) {
        if ( !this.events[event] ) this.events[event] = [];
        this.events[event].push(callback);
    }

    off(event) {
        if ( this.events[event] ) delete this.events[event];
    }
}

module.exports = Marker;