/**
 * Created by Philipe on 14/03/2016.
 */
'use strict';

class MarkerOptions {
    constructor(options) {
        this.anchorPoint    = options.anchorPoint   || 0;
        this.animation      = options.animation     || 'DROP'; //[DROP | BOUNCE]
        this.attribution    = options.attribution   || undefined;
        this.clickable      = options.clickable     || true;
        this.crossOnDrag    = options.crossOnDrag   || true;
        this.cursor         = options.cursor        || 'pointer';
        this.draggable      = options.draggable     || false;
        this.icon           = options.icon          || undefined;
        this.label          = options.label         || undefined; //google.maps.MarkerLabel object
        this.map            = options.map           || undefined; //google.maps.Map object or undefined to remove marker
        this.opacity        = options.opacity       || 1; //0.0..1.0
        this.optimized      = options.optimized     || false;
        this.place          = options.place         || undefined; //google.maps.MarkerPlace object
        this.position       = options.position      || undefined; //google.maps.LatLng - Required*******
        this.shape          = options.shape         || undefined;//google.maps.MarkerShape object
        this.title          = options.title         || '';
        this.visible        = options.visible       || true;
        this.zIndex         = options.zIndex        || undefined;

        return this;
    }

}

module.exports = MarkerOptions;